#!/usr/bin/bash

DOINIT_FILE="/home/wineuser/.init"
if [ -f "$DOINIT_FILE" ]; then
    exec "$@"
else 
    echo "$DOINIT_FILE does not exist"
    /opt/dxvk/setup_dxvk.sh install    
    touch $DOINIT_FILE
    exec "$@"

fi
